/**
 * This page class used to populate vehicle hub page and submit vehicle details
 * 
 * @author chinni
 */
package com.selenium.pages;

public class VehicleHubPage {

	public void populatePolicyLevelDriverDetails() {
		
		VehicleInfoPage vehicleInfo = new VehicleInfoPage();
		vehicleInfo.populateVehicleInfoDetails();
		
		VehicleDetailsPage vehicleDetails = new VehicleDetailsPage();
		vehicleDetails.populateVehicleDetails();
		
		VehicleDiscountsPage vehicleDiscounts = new VehicleDiscountsPage();
		vehicleDiscounts.populateVehicleDiscounts();
		
		System.out.println("Vehicles have been added to the policy");
	}
}

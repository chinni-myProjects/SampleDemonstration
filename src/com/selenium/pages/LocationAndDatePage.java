/**
 * This class is used to populate location and date details
 * 
 * @author chinni
 */
package com.selenium.pages;

public class LocationAndDatePage {

	/**
	 * method to select location
	 */
	public void selectLocation() {
		System.out.println("method to select location");
	}
	
	/**
	 * method to enter start date
	 */
	public void selectDate() {
		System.out.println("metohd to enter date from date picker");
	}
	
}

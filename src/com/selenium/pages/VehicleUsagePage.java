/**
 * this page class used to populate vehicle address, usage and mileage details
 * 
 * @author chinni
 */
package com.selenium.pages;

public class VehicleUsagePage {

	public void populateVehicleAddress() {
		System.out.println("address has been updated");
	}
	
	public void selectPrimaryDriver() {
		System.out.println("updated primary driver");
	}
	
	public void selectVehicleUsage() {
		System.out.println("updated vehicle usage");
	}
	
	public void enterMileage() {
		System.out.println("entered vehicle usage");
	}
}

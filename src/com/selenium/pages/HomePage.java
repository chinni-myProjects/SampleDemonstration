/**
 * this class used to validate home page
 * 
 * @author chinni
 */
package com.selenium.pages;

public class HomePage {

	/**
	 * method used to validate home page buttton
	 */
	public void homePageValidation() {
		System.out.println("home page has been validated");
	}
	
}

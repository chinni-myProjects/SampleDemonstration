/**
 * This page class has been used to populate driver prior insurance details
 * 
 * @author chinni
 */
package com.selenium.pages;

public class DriverPriorInsurancePage {

	public void populatePriorInsuranceDetails() {
		System.out.println("Prior Insurance Details have been updated");
	}
}

/**
 * this page class used to populate driver hub page and submit driver details
 * 
 * @author chinni
 */
package com.selenium.pages;

public class DriverHubPage {

	
	public void populatePolicyLevelDriverDetails() {
		
		DriverDetailsPage driverDetails = new DriverDetailsPage();
		driverDetails.populateDriverDetails();
		
		DriverPriorInsurancePage priorInsuranceDetails = new DriverPriorInsurancePage();
		priorInsuranceDetails.populatePriorInsuranceDetails();
		
		DriverDiscountsPage driverDiscounts = new DriverDiscountsPage();
		driverDiscounts.populateDriverDiscounts();
		
		System.out.println("Driver have been added to the policy");
	}
}

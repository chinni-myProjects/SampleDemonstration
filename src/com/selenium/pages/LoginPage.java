/**
 * this class is used to submit login details
 * 
 * @author chinni
 */
package com.selenium.pages;

public class LoginPage {

	/**
	 * method used to enter user id
	 */
	public void enterUserName() {
		System.out.println("enter user id:");
	}
	
	/**
	 * method used to enter password
	 */
	public void enterPassword() {
		System.out.println("enter password");
	}
	
}
